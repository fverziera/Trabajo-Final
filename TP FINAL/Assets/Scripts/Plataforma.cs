using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plataforma : MonoBehaviour
{

    bool tengoQueBajar = false;
    public int rapidez = 10;
    public float alturaMax;
    public float alturaMin;

    void Update()
    {
        if (transform.position.y >= alturaMax)
        {
            tengoQueBajar = true;
        }
        if (transform.position.y <= alturaMin)
        {
            tengoQueBajar = false;
        }

        if (tengoQueBajar)
        {
            Bajar();
        }
        else
        {
            Subir();
        }

    }

    void Subir()
    {
        transform.position += transform.up * rapidez * Time.deltaTime;
    }

    void Bajar()
    {
        transform.position -= transform.up * rapidez * Time.deltaTime;
    }
}
