using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Escena : MonoBehaviour
{
    public GameObject Jugador;
    public ControlJuego controlJuego;


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            controlJuego.ComenzarJuego();
            Time.timeScale = 1;
        }
        if (Jugador.transform.position.y <= -3)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            controlJuego.ComenzarJuego();
        }

    }

}
