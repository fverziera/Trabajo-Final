using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlataformaX : MonoBehaviour
{
    bool tengoQueBajar = false;
    public int rapidez = 10;
    public float alturaMax;
    public float alturaMin;

    void Update()
    {
        if (transform.position.x >= alturaMax)
        {
            tengoQueBajar = true;
        }
        if (transform.position.x <= alturaMin)
        {
            tengoQueBajar = false;
        }

        if (tengoQueBajar)
        {
            Bajar();
        }
        else
        {
            Subir();
        }

    }

    void Subir()
    {
        transform.position += transform.right * rapidez * Time.deltaTime;
    }

    void Bajar()
    {
        transform.position -= transform.right * rapidez * Time.deltaTime;
    }
}
